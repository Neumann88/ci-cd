package main

import "fmt"

func Add(x, y int) (res int) {
	return x + y
}

// Subtract subtracts two integers
func Subtract(x, y int) (res int) {
	return x - y
}

func main() {
	for idx, _ := range []int{1, 2, 3} {
		fmt.Println(idx)
	}
}
